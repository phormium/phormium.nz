# phormium.nz

The website for Phormium.

## Building

```shell
$ bundle install # install dependencies
$ bundle exec jekyll build
```

## License

The textual content and code of `phormium.nz` is licensed under the
[Creative Commons BY-NC-SA 4.0 International license](./LICENSE.md).

Please note that this license _does not cover_ use of the "Phormium"
branding, logos, or other art.
